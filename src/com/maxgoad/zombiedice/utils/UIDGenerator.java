package com.maxgoad.zombiedice.utils;

import java.util.Set;
import java.util.HashSet;
import java.util.Random;

public class UIDGenerator {

	private static Random rng = new Random();
	private static Set<Integer> used = new HashSet<>();
	
	public static void reset() {
		used.clear();
	}
	
	public static int generate() {
		int newUID = -1;
		
		do {
			newUID = rng.nextInt(100);
		} while (used.contains(newUID));
		
		used.add(newUID);
		
		return newUID;
	}
}

