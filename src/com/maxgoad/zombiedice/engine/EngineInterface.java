package com.maxgoad.zombiedice.engine;

public interface EngineInterface {
	
	public String playGame();
	public void resetGameState();
	
	public boolean addPlayer(int uid, String playerName);
	public boolean removePlayer(int uid);
}
