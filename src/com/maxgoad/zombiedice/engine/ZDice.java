package com.maxgoad.zombiedice.engine;

import java.util.Random;

public class ZDice {

	public enum ZDColour {
		GREEN,
		YELLOW,
		RED;
	}
	
	public enum ZDFace {
		BRAINS,
		RUNNER,
		BLAST;
	}

	private ZDColour colour;
	private ZDFace   currentFace;
	
	////////////////
	//Constructors//
	////////////////
	public ZDice() {
		this.colour 	 = ZDColour.YELLOW;
		this.currentFace = ZDFace.RUNNER;
	}
	
	public ZDice(ZDColour colour) {
		this.colour 	 = colour;
		this.currentFace = ZDFace.RUNNER;
	}
	
	public ZDice(ZDColour colour, ZDFace currentFace) {
		this.colour 	 = colour;
		this.currentFace = currentFace;
	}

	public ZDice(ZDice d) {
		this.colour = d.colour;
		this.currentFace = d.currentFace;
	}

	////////////
	//Main fns//
	////////////
	public void roll() {		
		Random rng = new Random();
		byte b = (byte)(Math.abs(rng.nextInt()) % 6);
		
		switch(b) {
			case 0:
			case 1:
				//Two guaranteed Runners on each die
				this.currentFace = ZDFace.RUNNER;
				break;
			case 2:
				//One guaranteed Brain on each die
				this.currentFace = ZDFace.BRAINS;
				break;
			case 3:
				//One guaranteed Blast on each die
				this.currentFace = ZDFace.BLAST;
				break;
			
			case 4:
			case 5:
				//Dice dependent faces:
				//Green  == 2 extra Brains
				//Red 	 == 2 extra Blasts
				//Yellow == 1 of each
				if (this.colour == ZDColour.GREEN) {
					this.currentFace = ZDFace.BRAINS;
				}
				else if (this.colour == ZDColour.RED) {
					this.currentFace = ZDFace.BLAST;
				}
				else { //Special Case Yellow
					if (b == 4)
						this.currentFace = ZDFace.BRAINS;
					else
						this.currentFace = ZDFace.BLAST;
				}
				
				break;
		}
	}

	///////////////////
	//Getters/Setters//
	///////////////////
	public ZDColour getColour() {
		return colour;
	}

	public ZDFace getCurrentFace() {
		return currentFace;
	}

	public void setColour(ZDColour colour) {
		this.colour = colour;
	}

	public void setCurrentFace(ZDFace currentFace) {
		this.currentFace = currentFace;
	}

}
