package com.maxgoad.zombiedice.engine;

import java.util.*;

import com.maxgoad.zombiedice.engine.ZDice.*;
import com.maxgoad.zombiedice.server.ZDServerSettings;

public class ZDGameState {

	public static final int NOONE = -1;
	
	private int 	currentPlayerUID;
	private byte 	scoreToWin;
	
	private ArrayList<ZDPlayer> players;
	private ArrayList<ZDice>  	scoreDice;
	private ArrayList<ZDice>  	deathDice;
	private ArrayList<ZDice>	fieldDice;

	//////////////////
	//Singleton Code//
	//////////////////
	private static ZDGameState instance = new ZDGameState();
	public static ZDGameState getInstance() {
		return instance;
	}
	
	////////////////
	//Constructors//
	////////////////
	private ZDGameState() {
		this.currentPlayerUID = NOONE;
		this.scoreToWin 	  = ZDServerSettings.dSCORE; //Default score: 13
		
		this.players   = new ArrayList<>();
		this.scoreDice = new ArrayList<>();
		this.deathDice = new ArrayList<>();
		this.fieldDice = new ArrayList<>();
	}
	
	////////////
	//Main fns//
	////////////
	public void resetGameState(ZDServerSettings settings) {
		this.scoreToWin = settings.getScoreToWin();
		
		this.scoreDice.clear();
		this.deathDice.clear();
		this.fieldDice.clear();
	}

	///////////////////
	//Getters/Setters//
	///////////////////
	public int getCurrentPlayerUID() {
		return currentPlayerUID;
	}
	
	public void incrementCurrentPlayer() {
		for(int i = 0; i < players.size(); i++) {
			ZDPlayer p = players.get(i);
			if (p.getUid() == currentPlayerUID) {
				int nextIndex = (i+1)%players.size();
				setCurrentPlayerUID(players.get(nextIndex).getUid());
				return;
			}
		}
	}
	
	public void randomizeCurrentPlayer() {
		Random rng = new Random();
		byte b = (byte)(Math.abs(rng.nextLong()) % players.size());
		setCurrentPlayerUID(players.get(b).getUid());
	}

	public void setCurrentPlayerUID(int currentPlayerUID) {
		this.currentPlayerUID = currentPlayerUID;
	}

	public byte getScoreToWin() {
		return scoreToWin;
	}

	public void setScoreToWin(byte scoreToWin) {
		this.scoreToWin = scoreToWin;
	}
	
	public ArrayList<ZDPlayer> getPlayers() {
		return players;
	}
	
	public boolean addPlayer(ZDPlayer player) {
		return this.players.add(player);
	}
	
	public ZDPlayer removePlayer(int uid) {
		for (ZDPlayer p : players) {
			if (p.getUid() == uid) {
				players.remove(p);
				
				if (p.getUid() == currentPlayerUID) {
					currentPlayerUID = NOONE;
				}
				
				return p;
			}
		}
		return null;
	}
	
	public ZDPlayer getCurrentPlayer() {
		for (ZDPlayer p : players) {
			if (p.getUid() == currentPlayerUID) {
				return p;
			}
		}
		return null;
	}
	
	public ZDPlayer getHighestScorer() {
		ZDPlayer highestPlayer = null;
		byte highestScore = -1;
		for (ZDPlayer p : players) {
			if (p.getScore() > highestScore) {
				highestPlayer = p;
				highestScore = p.getScore();
			}
		}
		return highestPlayer;
	}

	public ArrayList<ZDice> getScoreDice() {
		return scoreDice;
	}

	public void addScoreDice(ZDice dice) {
		this.scoreDice.add(dice);
	}

	public ArrayList<ZDice> getDeathDice() {
		return deathDice;
	}

	public void addDeathDice(ZDice dice) {
		this.deathDice.add(dice);
	}

	public ArrayList<ZDice> getFieldDice() {
		return fieldDice;
	}

	public void addFieldDice(ZDice dice) {
		this.fieldDice.add(dice);
	}
	
	////////////////////
	//STATIC FUNCTIONS//
	////////////////////
	
	//Static Encrypt
	public static String encryptStateImage(ZDGameState image) {
		String result = "";
		
		result += image.scoreToWin + "!";
		result += image.currentPlayerUID + "@";
		
		for (ZDPlayer p : image.getPlayers()) {
			result += p.getName() + ","
					+ p.getUid() + ","
					+ p.getScore() + ","
					+ p.getWins() + "|";
		}
		result += "#";
		
		for (ZDice d : image.getScoreDice()) {
			result += d.getColour() + "," + d.getCurrentFace() + "|";
		}
		result += "$";

		for (ZDice d : image.getDeathDice()) {
			result += d.getColour() + "," + d.getCurrentFace() + "|";
		}
		result += "%";

		for (ZDice d : image.getFieldDice()) {
			result += d.getColour() + "," + d.getCurrentFace() + "|";
		}
		result += "^";
		
		return result;
	}
	
	//Static Decrypt
	public static ZDGameState decryptStateImage(String imageString) {
		ZDGameState result = new ZDGameState();
		
		//Score to win
		String[] splitter = imageString.split("!");
		result.setScoreToWin(Byte.parseByte(splitter[0]));
		
		//Current Player UID
		splitter = splitter[1].split("@");
		result.setCurrentPlayerUID(Integer.parseInt(splitter[0]));
		
		//Players
		splitter = splitter[1].split("#");
		String[] playerSplitter = splitter[0].split("\\|");
		for (String p : playerSplitter) {
			if (p.equals(""))
				continue;
			
			String[] playerSplit = p.split(",");
			
			ZDPlayer newPlayer = new ZDPlayer();
			newPlayer.setName (playerSplit[0]);
			newPlayer.setUid  (Integer.parseInt(playerSplit[1]));
			newPlayer.setScore(Byte.parseByte(playerSplit[2]));
			newPlayer.setWins (Byte.parseByte(playerSplit[3]));
			result.addPlayer(newPlayer);
		}
		
		//Score Dice
		splitter = splitter[1].split("\\$");
		String[] diceSplitter = splitter[0].split("\\|");
		for (String d : diceSplitter) {
			if (d.equals(""))
				continue;
			
			String[] diceSplit = d.split(",");
			
			ZDice newDie = new ZDice();
			newDie.setColour(ZDColour.valueOf(diceSplit[0]));
			newDie.setCurrentFace(ZDFace.valueOf(diceSplit[1]));
			result.addScoreDice(newDie);
		}
		
		//Death Dice
		splitter = splitter[1].split("%");
		diceSplitter = splitter[0].split("\\|");
		for (String d : diceSplitter) {
			if (d.equals(""))
				continue;
			
			String[] diceSplit = d.split(",");
			
			ZDice newDie = new ZDice();
			newDie.setColour(ZDColour.valueOf(diceSplit[0]));
			newDie.setCurrentFace(ZDFace.valueOf(diceSplit[1]));
			result.addDeathDice(newDie);
		}
		
		//Field Dice
		splitter = splitter[1].split("\\^");
		if (splitter.length > 0) {
			diceSplitter = splitter[0].split("\\|");
			for (String d : diceSplitter) {
				if (d.equals(""))
					continue;
				
				String[] diceSplit = d.split(",");
				
				ZDice newDie = new ZDice();
				newDie.setColour(ZDColour.valueOf(diceSplit[0]));
				newDie.setCurrentFace(ZDFace.valueOf(diceSplit[1]));
				result.addFieldDice(newDie);
			}
		}
		
		return result;
	}
	
	public static ZDGameState buildMinimalState() {
		ZDGameState minimalImage = new ZDGameState();
		
		minimalImage.addPlayer(new ZDPlayer("Player One", 1));
		minimalImage.addPlayer(new ZDPlayer("Player Two", 2));
		
		return minimalImage;
	}
	
	public static ZDGameState buildTestState() {
		ZDGameState testImage = new ZDGameState();
		
		ZDPlayer testPlayer01 = new ZDPlayer();
		testPlayer01.setName("Player One");
		testPlayer01.setUid(1);
		testPlayer01.setScore((byte)99);
		testPlayer01.setWins((byte)12);

		ZDice die01 = new ZDice(ZDColour.GREEN, ZDFace.BRAINS);
		ZDice die02 = new ZDice(ZDColour.YELLOW, ZDFace.BRAINS);
		ZDice die03 = new ZDice(ZDColour.RED, ZDFace.BRAINS);
		
		ZDice die11 = new ZDice(ZDColour.GREEN, ZDFace.RUNNER);
		ZDice die12 = new ZDice(ZDColour.YELLOW, ZDFace.RUNNER);
		ZDice die13 = new ZDice(ZDColour.RED, ZDFace.RUNNER);
		
		ZDice die21 = new ZDice(ZDColour.GREEN, ZDFace.BLAST);
		ZDice die22 = new ZDice(ZDColour.YELLOW, ZDFace.BLAST);
		ZDice die23 = new ZDice(ZDColour.RED, ZDFace.BLAST);
		
		
		testImage.setCurrentPlayerUID(12345);
		testImage.setScoreToWin((byte)13);
		testImage.addPlayer(testPlayer01);
		testImage.addPlayer(new ZDPlayer("Player Two", 2));
		testImage.addPlayer(new ZDPlayer("Player Three", 3));
		testImage.addPlayer(new ZDPlayer("Player Four", 4));
		testImage.addPlayer(new ZDPlayer("Player Five", 5));
		testImage.addPlayer(new ZDPlayer("Player Six", 6));
		testImage.addPlayer(new ZDPlayer("Player Seven", 7));
		testImage.addPlayer(new ZDPlayer("Player Eight", 8));
		
		testImage.addScoreDice(die01);
		testImage.addScoreDice(die02);
		testImage.addScoreDice(die03);
		testImage.addScoreDice(die01);
		testImage.addScoreDice(die01);
		testImage.addScoreDice(die01);
		testImage.addScoreDice(die02);
		testImage.addScoreDice(die02);
		testImage.addScoreDice(die02);
		testImage.addScoreDice(die03);
		testImage.addScoreDice(die03);
		testImage.addScoreDice(die03);
		testImage.addScoreDice(die03);
		testImage.addScoreDice(die02);
		testImage.addScoreDice(die01);
		
		testImage.addDeathDice(die21);
		testImage.addDeathDice(die22);
		testImage.addDeathDice(die23);
		
		testImage.addFieldDice(die11);
		testImage.addFieldDice(die12);
		testImage.addFieldDice(die13);
		
		return testImage;
	}
	
	//Function used to test encrypt/decrypt
	public static void main(String[] args) {
		ZDGameState testImage = buildTestState();
		
		String testEncrypt = ZDGameState.encryptStateImage(testImage);
		
		System.out.println(testEncrypt);
		
		ZDGameState newTestImage = ZDGameState.decryptStateImage(testEncrypt);
		String newTestEncrypt = ZDGameState.encryptStateImage(newTestImage);
		
		System.out.println(newTestEncrypt);
		
		System.out.println(testEncrypt.equals(newTestEncrypt));
		
	}
		
		

}
