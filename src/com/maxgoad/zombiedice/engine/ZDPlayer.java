package com.maxgoad.zombiedice.engine;

public class ZDPlayer {

	private String name;
	private int	   uid;
	
	private byte   score;
	private byte   wins;
	private byte   turnNum;
	
	////////////////
	//Constructors//
	////////////////
	public ZDPlayer() {
		this.name  = "";
		this.uid   = -1;
		this.score = 0; 
		this.wins  = 0;
	}
	
	public ZDPlayer(String name, int uid) {
		this.name  = name;
		this.uid   = uid;
		this.score = 0; 
		this.wins  = 0;
	}

	////////////
	//Main fns//
	////////////
	public void resetScore() {
		score   = 0;
		turnNum = 0;
	}

	///////////////////
	//Getters/Setters//
	///////////////////
	public String 	getName() 	 { return name; }
	public int 		getUid() 	 { return uid; }
	public byte 	getScore() 	 { return score; }
	public byte 	getWins() 	 { return wins; }
	public byte		getTurnNum() { return turnNum; }
	
	public void setName(String name) { this.name  = name; }
	public void setUid(int uid) 	 { this.uid   = uid; }
	public void setScore(byte score) { this.score = score; }
	public void setWins (byte wins)  { this.wins  = wins; }
	public void addWin ()  			 { this.wins++; }
	public void setTurnNum (byte t)  { this.turnNum = t; }

}
