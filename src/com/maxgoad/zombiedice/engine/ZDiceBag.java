package com.maxgoad.zombiedice.engine;

import java.util.ArrayList;
import java.util.Random;

import com.maxgoad.zombiedice.engine.ZDice.ZDColour;
import com.maxgoad.zombiedice.server.ZDServerSettings;

public class ZDiceBag {

	ArrayList<ZDice> diceBag;
	
	////////////////
	//Constructors//
	////////////////
	public ZDiceBag(ZDServerSettings settings) {
		diceBag = new ArrayList<>();
		reset(settings);
	}

	////////////
	//Main fns//
	////////////
	public ZDice pick() {
		if (diceBag.isEmpty()) {
			return null;
		}
		
		Random rng = new Random(System.currentTimeMillis());
		return diceBag.remove((Math.abs(rng.nextInt()) % diceBag.size()));
	}
	
	public void reset(ZDServerSettings settings) {
		//Green Dice
		for (int i = 0; i < settings.getNumGreenDice(); i++) {
			diceBag.add(new ZDice(ZDColour.GREEN));
		}
		
		//Yellow Dice
		for (int i = 0; i < settings.getNumYellowDice(); i++) {
			diceBag.add(new ZDice(ZDColour.YELLOW));
		}		
		
		//Red Dice
		for (int i = 0; i < settings.getNumRedDice(); i++) {
			diceBag.add(new ZDice(ZDColour.RED));
		}
	}
	
	public boolean isEmpty() {
		return diceBag.isEmpty();
	}

}
