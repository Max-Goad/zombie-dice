package com.maxgoad.zombiedice.engine;

import java.util.ArrayList;

import com.maxgoad.zombiedice.server.*;

public class ZDEngine implements EngineInterface {

	private ServerInterface 	server;
	private ZDServerSettings	gameSettings;
	private ZDGameState 		gameState;
	private ZDiceBag			diceBag;
	
	private boolean doLog = true;
	
	////////////////
	//Constructors//
	////////////////
	public ZDEngine(ServerInterface server, ZDServerSettings settings) {
		this.server 		= server;
		this.gameSettings 	= settings;
		this.gameState 		= ZDGameState.getInstance();
		this.diceBag 	    = new ZDiceBag(settings);
		
		gameState.resetGameState(settings);
	}	

	////////////
	//Main fns//
	////////////
	@Override
	public String playGame() {

		server.updateAllStates(ZDGameState.encryptStateImage(gameState));
		
		ArrayList<ZDice> rolledDice = new ArrayList<>(3),
						 runnerDice = new ArrayList<>(3);
		
		boolean gameOver = false,
				roundOver = false,
				overtime = false;
		
		ArrayList<ZDPlayer> overtimePlayers = new ArrayList<>();
		
		//Game Loop
		while (!gameOver) {

			//println("Game Lewp");
			//Round Loop
			while(!roundOver) {

				//println("Round Lewp");
				
				//First check if enough players are still playing
				if (gameState.getPlayers().size() < 2) { 
					//TODO: Fix the magic number above plz
					gameOver = true;
					break;
				}
				
				//If no player selected to play
				if (gameState.getCurrentPlayerUID() == ZDGameState.NOONE) {
					//Randomly choose a player to go
					gameState.randomizeCurrentPlayer();
					server.updateAllStates(ZDGameState.encryptStateImage(gameState));
					server.indicateTurn(gameState.getCurrentPlayerUID());
					println(gameState.getCurrentPlayer().getName() + "(" + gameState.getCurrentPlayerUID() + ")" + " goes next! Waiting for turn action...");
				}
				
				//Get turn action
				String turnAction = server.getTurnAction(gameState.getCurrentPlayerUID());
				
				println("Received turn action: " + turnAction);
				
				if (turnAction == null) {
					continue;
				}
				
				boolean nextTurn = false;
				
				//Parse turn action
				switch(turnAction) {
				
				
					case "ROLL":
						
						//Step 1: Pick up to three dice
						while(rolledDice.size() < 3) {
							ZDice die = diceBag.pick();
							
							//If you got a die, add it to list and keep going
							if (die != null) {
								rolledDice.add(die);
							}
							//If you found no dice, stop looking
							else {
								break;
							}
						}
						
						//Step 2: Roll die and place into appropriate place(s)
						for (int i = 0; i < rolledDice.size(); i++) {		
							ZDice d = rolledDice.get(i);
							
							d.roll();
							gameState.addFieldDice(d);	//Always add to field
							
							switch(d.getCurrentFace()) {
								case BRAINS:
									gameState.addScoreDice(d);
									break;
								case BLAST:
									gameState.addDeathDice(d);
									break;
								case RUNNER:
									runnerDice.add(d);
									break;
							}
						}
						
						///Turn outcome decisions
						
						//Step 3: Check if the player has lost
						if(gameState.getDeathDice().size() > 2) {
							//If they've lost, go to the next player without adding their score
							nextTurn = true;
							rolledDice.clear();
							runnerDice.clear();
						}
						
						//Step 4: Check if the player has exhausted the dice bag
						else if (diceBag.isEmpty()) {
							//Wow! Add their score and move onto the next player
							ZDPlayer p = gameState.getCurrentPlayer();
							p.setScore((byte)(p.getScore() + gameState.getScoreDice().size()));
							nextTurn = true;
							rolledDice.clear();
							runnerDice.clear();
						}
						
						//Step 5: If nothing special happened, clear field, re-use runner dice and continue!
						else {
							server.updateAllStates(ZDGameState.encryptStateImage(gameState));
							gameState.getFieldDice().clear();
							rolledDice.clear();
							rolledDice.addAll(runnerDice);
							runnerDice.clear();
						}
						
						break;
						
						
					case "QUIT":
						
						//Quitting adds your current score to your total, but ends your turn
						ZDPlayer p = gameState.getCurrentPlayer();
						p.setScore((byte)(p.getScore() + gameState.getScoreDice().size()));
						nextTurn = true;
						
						break;
						
					default:
						println("What the fuck");
						break;
				}
				
				//Turn is resolved
				
				//Overtime special code
				if (overtime && nextTurn) {
					ZDPlayer currentPlayer = gameState.getCurrentPlayer();
					overtimePlayers.remove(currentPlayer);
					
					if (overtimePlayers.isEmpty()) {
						//All extra turns have been taken! The round is now over
						roundOver = true;
					}
					
				}
				//Check if anyone has won yet
				else if (gameState.getHighestScorer().getScore() >= gameSettings.getScoreToWin()) {
					//Give any players still left to go a chance to beat the score!
					println("Overtime has begun!");
					overtime = true;
					
					byte overtimeTurnNum = gameState.getHighestScorer().getTurnNum();
					for (ZDPlayer p : gameState.getPlayers()) {
						if (p.getTurnNum() < overtimeTurnNum) {
							overtimePlayers.add(p);
						}
					}
				}
				
				if (nextTurn) {
					nextTurn();
				}
				
				
			}
			
			//Game Loop Cont.
			gameState.getHighestScorer().addWin();
			
			if (gameState.getHighestScorer().getWins() >= gameSettings.getNumRounds()) {
				gameOver = true;
			}
			else {
				gameState.resetGameState(gameSettings);
				gameState.randomizeCurrentPlayer();
				server.indicateTurn(gameState.getCurrentPlayerUID());
			}
		}
		
		return gameState.getHighestScorer().getName();
	}

	@Override
	public void resetGameState() {
		gameState.resetGameState(gameSettings);
	}

	@Override
	public boolean addPlayer(int uid, String playerName) {
		boolean result = gameState.addPlayer(new ZDPlayer(playerName, uid));
		if (result) {
			println("Registered player \"" + playerName + "\" with UID " + uid);
		}
		return result;
	}

	@Override
	public boolean removePlayer(int uid) {
		ZDPlayer result = gameState.removePlayer(uid);
		if (result != null) {
			println("Deregistered player \"" + result.getName() + "\" with UID " + uid);
		}
		return result != null;
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	private void nextTurn() {
		println("Next Turn Called!");
		
		//Function Order Matters!!!
		gameState.incrementCurrentPlayer();								  //Signal next player to go
		server.updateAllStates(ZDGameState.encryptStateImage(gameState)); //Update BEFORE clear to ensure last board state is shown
		server.indicateTurn(gameState.getCurrentPlayerUID());			  //Indicate player's turn has started to appropriate client
		gameState.resetGameState(gameSettings);							  //Reset board state behind the scenes
		diceBag.reset(gameSettings);									  //Reset dice bag to full
		
		println(gameState.getCurrentPlayer().getName() + "(" + gameState.getCurrentPlayerUID() + ")" + " goes next! Waiting for turn action...");
	}
	
	private void println(String s) {
		if (doLog) {
			System.out.println(this.getClass().getSimpleName() + ": " + s);
		}
	}
	
}
