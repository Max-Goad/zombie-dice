package com.maxgoad.zombiedice.client;

public class ZDClientSettings {

	///////////
	//Presets//
	///////////
	public static final ZDClientSettings CLIENT_SETTING_DEFAULT = new ZDClientSettings();
	public static final ZDClientSettings CLIENT_LOCAL_TEST = new ZDClientSettings("Max", "127.000.000.001", 5050);
	
	//////////////
	//Local vars//
	//////////////
	private String playerName;
	private String serverIP;

	private int	   uid;
	private int	   serverPort;
	
	////////////////
	//Constructors//
	////////////////
	public ZDClientSettings() {
		this.playerName = null;
		this.serverIP   = null;
		
		this.uid 		= -1;
		this.serverPort = -1;
	}
	
	public ZDClientSettings(String playerName, String serverIP, int serverPort) {
		this.playerName = playerName;
		this.serverIP   = serverIP;
		
		this.uid 		= -1;
		this.serverPort = serverPort;
		
	}

	///////////////////
	//Getters/Setters//
	///////////////////
	public String getPlayerName() {
		return playerName;
	}

	public String getServerIP() {
		return serverIP;
	}

	public int getUid() {
		return uid;
	}

	public int getServerPort() {
		return serverPort;
	}
	
	public boolean complete() {
		return this.playerName != null
			&& this.serverIP != null
			&& this.serverPort != -1;
	}

	public void setUID(int uid) {
		this.uid = uid;
	}
}
