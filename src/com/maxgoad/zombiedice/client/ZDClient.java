package com.maxgoad.zombiedice.client;

import java.net.Socket;
import java.util.*;

import com.maxgoad.zombiedice.graphics.GraphicalUserInterface;
import com.maxgoad.zombiedice.graphics.ZDUserInterface;
import com.maxgoad.zombiedice.server.message.*;

public class ZDClient extends Messenger {
	
	public static final int MIN_PORT_NUM = 1024;
	public static final int MAX_PORT_NUM = 10240;
	public static final int MIN_STRING_SIZE = 3;
	public static final int MAX_STRING_SIZE = 20;
	
	public static final int DEFAULT_PORT_NUMBER = 5050;
	
	public static void main(String[] args) {
		ZDClient client = new ZDClient();
		client.setSettings(ZDClientSettings.CLIENT_LOCAL_TEST);
		client.start();
		
		System.exit(0);
	}
	
	///

	private ZDClientSettings settings = null;
	private GraphicalUserInterface userInterface = null;
	
	//Pre-UI Code:
	//private Scanner inputScanner = null;
	private Socket socket = null;
	
	private boolean isRunning;
	private boolean doLog;
	
	
	////////////////
	//Constructors//
	////////////////
	public ZDClient() {
		super();
		
		this.isRunning = false;
		this.doLog = true;
		
		userInterface = new ZDUserInterface(this);
	}

	////////////
	//Main fns//
	////////////
	public void start() {
		println("Starting up ZombieDiceClient...");
		
		isRunning = true;
		
		//Step 1: Initialize Client
		initialize();
		
		//Step 2: Open Socket to Server
		openConnection();
		
		//Step 3: Verify Connection
		verifyConnection();
		
		while (this.isRunning) {
			//Step 4: Wait for Game Start
			waitForGameStart();
			
			//Step 5: Launch UI
			initializeUserInterface();
			
			//Step 6: Start Game Loop
			String winner = gameLoop();
			
			//Step 7: Shutdown UI, Announce Winner and Prompt Player to Play Again
			promptToPlayAgain(winner);
		}
		
		//Final Step: Stop Client		
		fullQuit();
	}
	
	public void stop() {
		isRunning = false;
	}
	
	public void fullQuit() {
		println("Shutting down ZombieDiceClient...");
		System.exit(0);
	}
	////////////////////
	//Client Logic Fns//
	////////////////////
	private void initialize() {
		
		//TODO: Is this loop necessary??
		do {
			settings = userInterface.initialize(settings);
		} while (!settings.complete());
		
		/* Pre-UI Code
		 * 
		inputScanner = new Scanner(System.in);
		
		if (settings == null) {
			settings = ZDClientSettings.CLIENT_SETTING_DEFAULT;
		}
		
		while (!settings.complete()) {
			String s1 = "Please enter your name:";
			String iName = getInputString(inputScanner, s1, MIN_STRING_SIZE, MAX_STRING_SIZE);
			
			String s2 = "Please enter the IP of the server you wish to join:";
			String iServerIP = getInputString(inputScanner, s2, MIN_STRING_SIZE, MAX_STRING_SIZE);
			
			String s3 = "Please enter the port you wish to connect on ("+MIN_PORT_NUM+"-"+MAX_PORT_NUM+")";
			int iServerPort = getInputInt(inputScanner, s3, MIN_PORT_NUM, MAX_PORT_NUM);
			
			settings = new ZDClientSettings(iName, iServerIP, iServerPort);
		}
		*/
	}
	
	private void openConnection() {
		if (socket == null) {
			try {
				String ip = this.settings.getServerIP();
				int port = this.settings.getServerPort();
				
				println("Attempting connection to server at IP " + ip + ":" + port + "...");
				
				this.socket = new Socket(ip, port);
				
			} catch (Exception e) {
				println(e.getMessage());
				this.stop();
				return;
			}
		}
		
		super.initializeStreams(socket);
	}
	
	private void verifyConnection() {
		//Wait for registration request from server
		Message request = waitForMessage(Message.Type.CLIENT_REGISTRATION_REQUEST);
		if (!isValidMessage(request)) {
			println("establishMessageConnection() failed!"); 
			this.stop();
			return;
		}
		
		//Craft and send response
		Message response = new Message(Message.Type.CLIENT_REGISTRATION_RESPONSE, this.settings.getPlayerName());
		if (!sendMessage(response)) {
			this.stop();
			return;
		}
		
		//Wait for confirmation message
		Message confirmation = waitForMessage(Message.Type.CLIENT_REGISTRATION_CONFIRMATION);
		if (!isValidMessage(confirmation, true)) {
			println("establishMessageConnection() failed!"); 
			this.stop();
			return;
		}
		
		println("Connection to server successful!");
		
		this.settings.setUID(Integer.parseInt(confirmation.contents));
	}
	
	private void waitForGameStart() {
		
		Message serverMsg = null;
		
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(Message.Type.GAME_START);
		expectedTypes.add(Message.Type.CLIENT_CONNECT_UPDATE);
		
		do {
			serverMsg = waitForMessage(expectedTypes);
			if (serverMsg == null) {
				this.stop();
				return;
			}
			else if (serverMsg.type == Message.Type.CLIENT_CONNECT_UPDATE) {
				String[] split = serverMsg.contents.split("/");
				userInterface.updateStartStatus(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
			}
		} while (!isValidMessage(serverMsg) || !(serverMsg.type == Message.Type.GAME_START));
		
		/* Pre-UI Code
		 * 
		println("Waiting for server to start the game...");
		
		Message gameStartMessage = null;
		
		do {
			gameStartMessage = waitForMessage(Message.Type.GAME_START);
			if (gameStartMessage == null) {
				this.stop();
				return;
			}
		} while (!isValidMessage(gameStartMessage));	
		*/
	}
	
	private void initializeUserInterface() {
		userInterface.startGame(this.settings.getUid());
	}
	
	private String gameLoop() {

		String winningPlayer = null;
		boolean gameComplete = false;
		
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(Message.Type.GAME_UPDATE_STATE);
		expectedTypes.add(Message.Type.GAME_TURN_START);
		expectedTypes.add(Message.Type.GAME_TURN_ACTION_REQUEST);
		expectedTypes.add(Message.Type.GAME_END);
		expectedTypes.add(Message.Type.GAME_USER_DROP);
		
		while (!gameComplete) {
			
			Message receivedMsg  = waitForMessage(expectedTypes);
			if (!isValidMessage(receivedMsg)) {
				println("gameLoop() received message failed validation!"); 
				this.stop();
				return null;
			}
			
			switch (receivedMsg.type) {
				case GAME_UPDATE_STATE:
					userInterface.updateState(receivedMsg.contents);
					continue;
				case GAME_TURN_START:
					userInterface.indicateTurn();
					continue;
				case GAME_TURN_ACTION_REQUEST:
					String turnResponse = userInterface.getTurnAction();
					Message turnResponseMsg = new Message(Message.Type.GAME_TURN_ACTION_RESPONSE, turnResponse);
					if (!sendMessage(turnResponseMsg)) {
						println("Error! gameLoop() send message failed!");
						this.stop();
						return null;
					}
					continue;
				case GAME_END:
					winningPlayer = receivedMsg.contents;
					gameComplete = true;
					break;
				case GAME_USER_DROP:
					userInterface.indicatePlayerDrop(receivedMsg.contents);
					continue;
				default:
					println("Unexpected message type received in game loop! " + receivedMsg.type.toString());
					return null;
			}
		}
		
		return winningPlayer;
	}
	
	private void promptToPlayAgain(String winner) {

		Message serverNotification = null;
		
		if (userInterface.stopGame(winner)) {
			serverNotification = new Message(Message.Type.CLIENT_FINISHED_PLAYING);
			this.stop();
		}
		else {
			serverNotification = new Message(Message.Type.CLIENT_CONTINUE_PLAYING);
		}
		
		sendMessage(serverNotification);
		
		/* Pre-UI Code
		 *
		while (true) {
			
			println("Would you like to participate in another game? (Yes or No)");
			
			String rawcmd = inputScanner.nextLine();
			
			//Check string integrity
			if (rawcmd == null) {
				println("Unknown Error! Command string parsed to be null!");
				continue;
			}
			
			switch(rawcmd.toLowerCase()) {
				case "yes":
				case "y":
					serverNotification = new Message(Message.Type.CLIENT_CONTINUE_PLAYING);
					sendMessage(serverNotification);
					//TODO: Log something to user?
					return;
				case "no":
				case "n":
					serverNotification = new Message(Message.Type.CLIENT_FINISHED_PLAYING);
					sendMessage(serverNotification);
					//TODO: Log something to user?
					this.stop();
					return;
				default:
					println("Incorrect/Unknown input!");
					continue;
			}
		}
		*/
	}
	
	///////////////////////////
	//Getters/Setters/Helpers//
	///////////////////////////
	public void setSettings(ZDClientSettings settings) {
		this.settings = settings;
	}
	
	/* Pre-UI Code
	 *
	private String getInputString(Scanner scanner, String prompt, int minLength, int maxLength) {
		
		String result = "";
		
		println(prompt);
		
		while (result.equals("")) {
			
			result = scanner.nextLine(); 
			if (result.length() < minLength) {
				println("The text you have inputted is too small. Please enter something with at least " + minLength + " characters.");
				continue;
			}
			else if (result.length() > maxLength) {
				println("The text you have inputted is too large. Please enter something with at most " + maxLength + " characters.");
				continue;
			}
			
			if (!result.matches("^[a-zA-Z_0-9.]+$")) {
				println("Please do not use any special characters besides \'_\' and \'.\'");
			}
		}
		
		return result;
	}
	
	private int getInputInt(Scanner scanner, String prompt, int min, int max) {
		
		int result = Integer.MIN_VALUE;
		
		println(prompt);
		
		while (result == Integer.MIN_VALUE) {
			
			try { 
				result = Integer.parseInt(scanner.nextLine()); 
			}
			catch (NumberFormatException e) {
				println("Cannot parse input! Try again.");
			}
		}
		
		return result;
	}
	*/
	
	private void println(String s) {
		if (doLog) {
			System.out.println(this.getClass().getSimpleName() + ": " + s);
		}
	}
}
