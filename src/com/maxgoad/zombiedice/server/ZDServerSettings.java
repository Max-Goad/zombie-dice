package com.maxgoad.zombiedice.server;

public class ZDServerSettings {

	///////////
	//Presets//
	///////////
	public static final ZDServerSettings SERVER_SETTING_DEFAULT 	= new ZDServerSettings();
	public static final ZDServerSettings SERVER_SETTING_LARGE		= new ZDServerSettings((byte)8, (byte)1, (byte)13);
	public static final ZDServerSettings SERVER_LOCAL_TEST			= new ZDServerSettings((byte)2, (byte)1, (byte)13);
	
	public static final byte dPLAYERS 	= 4;
	public static final byte dROUNDS	= 1;
	public static final byte dSCORE 	= 13;
	public static final byte dGDICE 	= 6;
	public static final byte dYDICE		= 4;
	public static final byte dRDICE		= 3;
	
	//////////////
	//Local vars//
	//////////////	
	private byte numPlayers;
	private byte numRounds;
	private byte scoreToWin;
	
	private byte numGreenDice;
	private byte numYellowDice;
	private byte numRedDice;
	
	////////////////
	//Constructors//
	////////////////
	public ZDServerSettings() {
		this.numPlayers = dPLAYERS;
		this.numRounds 	= dROUNDS;
		this.scoreToWin = dSCORE;
		
		this.numGreenDice 	= dGDICE;
		this.numYellowDice 	= dYDICE;
		this.numRedDice 	= dRDICE;
	}

	public ZDServerSettings(byte numPlayers, byte numRounds, byte scoreToWin) {
		this.numPlayers = numPlayers;
		this.numRounds 	= numRounds;
		this.scoreToWin = scoreToWin;
		
		this.numGreenDice 	= dGDICE;
		this.numYellowDice 	= dYDICE;
		this.numRedDice 	= dRDICE;
	}

	public ZDServerSettings(byte numPlayers, byte numRounds, byte scoreToWin, 
			byte numGreenDice, byte numYellowDice, byte numRedDice) {
		this.numPlayers = numPlayers;
		this.numRounds 	= numRounds;
		this.scoreToWin = scoreToWin;
		
		this.numGreenDice 	= numGreenDice;
		this.numYellowDice 	= numYellowDice;
		this.numRedDice 	= numRedDice;
	}

	///////////////////
	//Getters/Setters//
	///////////////////
	public byte getNumPlayers() {
		return numPlayers;
	}

	public byte getNumRounds() {
		return numRounds;
	}

	public byte getScoreToWin() {
		return scoreToWin;
	}

	public byte getNumGreenDice() {
		return numGreenDice;
	}

	public byte getNumYellowDice() {
		return numYellowDice;
	}

	public byte getNumRedDice() {
		return numRedDice;
	}

}
