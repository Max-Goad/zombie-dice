package com.maxgoad.zombiedice.server.message;

import java.io.Serializable;

public class Message implements Serializable {

	private static final long serialVersionUID = 8352691928399750769L;

	///////////////////////
	//Type enum declaration
	///////////////////////
	public enum Type {
		//Relay setup messages
		CLIENT_REGISTRATION_REQUEST,		//Server -> Client
		CLIENT_REGISTRATION_RESPONSE,		//Client -> Server; Player info
		CLIENT_REGISTRATION_CONFIRMATION,	//Server -> Client
		CLIENT_FINISHED_PLAYING,			//Client -> Server
		CLIENT_CONTINUE_PLAYING,			//Client -> Server
		CLIENT_CONNECT_UPDATE,				//Client -> Server; Connection Numbers
		CONNECTION_PING,					//Special type; Used to check connection; Is ignored always
		
		//Error messages (always set to true)
		ERROR_INCORRECT_MSG_SENT(true),
		ERROR_RETRY_INPUT(true),
		ERROR_UNEXPECTED(true),		
		//Test messages
		TEST_MESSAGE,
		TEST_ERROR(true),
		
		//Game-specific messages
		GAME_START,					//Server -> Client
		GAME_TURN_START,			//Server -> Client
		GAME_UPDATE_STATE,			//Server -> Client; Full state info
		GAME_USER_ERROR,			//Server -> Client
		GAME_USER_DROP,				//Server -> Client; Dropped player info
		//GAME_STRING_REQUEST,		//Server -> Client
		//GAME_STRING_RESPONSE,		//Client -> Server
		GAME_TURN_ACTION_REQUEST,	//Server -> Client
		GAME_TURN_ACTION_RESPONSE,	//Client -> Server; Playing cards, ending turn, withdrawing
		//GAME_IVANHOE_REQUEST,		//Server -> Client
		//GAME_IVANHOE_RESPONSE,	//Client -> Server; Yes/No
		GAME_END;					//Server -> Client
		
		public boolean isError;
		
		Type() {
			isError = false;
		}
		
		Type(boolean isError) {
			this.isError = isError;
		}
	}
	
	public Type type;
	public String contents;
	
	public Message(Type type) {
		this.type = type;
		this.contents = null;
	}
	
	public Message(Type type, String contents) {
		this.type = type;
		this.contents = contents;
	}
	
	@Override
	public String toString() {
		return "Message("+this.type+","+this.contents+")";
	}
}
