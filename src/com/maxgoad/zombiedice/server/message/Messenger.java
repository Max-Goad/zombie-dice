package com.maxgoad.zombiedice.server.message;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;

public class Messenger {

	private ObjectOutputStream outStream;
	private ObjectInputStream inStream;
	
	private boolean doLog = true;
	
	public Messenger(){
	}
	
	public boolean initializeStreams(Socket s) {
		try {
			outStream = new ObjectOutputStream(s.getOutputStream());
			outStream.flush();
			inStream = new ObjectInputStream(s.getInputStream());
			return true;
		} catch (IOException e) {
			println("initializeStreams ioe - " + e.getMessage());
			return false;
		}
	}
	
	public boolean sendMessage(Message msg) {		
		if (outStream == null) {
			println("OutStream not initialized!");
			return false;
		}
		
		try {
			//System.out.println("Send " + msg);
			outStream.writeObject(msg);
			return true;
		} catch (IOException e) {
			println("sendMessage ioe - " + e.getMessage());
			return false;
		}
	}
	
	public boolean sendPing() {
		return this.sendMessage(new Message(Message.Type.CONNECTION_PING));
	}

	public Message waitForMessage(Message.Type expectedType) {
		Set<Message.Type> typeSet = new HashSet<>();
		typeSet.add(expectedType);
		return waitForMessage(typeSet);
	}
	
	public Message waitForMessage(Set<Message.Type> expectedTypes) {
		if (inStream == null) {
			println("InStream not initialized!");
			return null;
		}
		
		if (expectedTypes.isEmpty()) {
			println("Expected type set is empty!");
			return null;
		}		
		
		Message receivedMsg = null;
		
		try {
			boolean receiveSuccess = false;
			while (!receiveSuccess) {
				receivedMsg = (Message)inStream.readObject();
				
				//We return either correct type messages OR error messages
				if (expectedTypes.contains(receivedMsg.type) || receivedMsg.type.isError) {
					receiveSuccess = true;
				} 
				//If we receive an unexpected ping, ignore it and try reading again.
				else if(receivedMsg.type == Message.Type.CONNECTION_PING) {
					continue;
				}
				//We reject messages that do not match what we are expecting
				//But are not error messages
				else {
					String expectedTypeString = "Expected:";
					for (Message.Type type : expectedTypes) {
						expectedTypeString += " ";
						expectedTypeString += type.toString();
					}
					
					Message incorrectTypeMsg = new Message(Message.Type.ERROR_INCORRECT_MSG_SENT, expectedTypeString);
					sendMessage(incorrectTypeMsg);
				}
			}
		} catch (SocketException e) {
			receivedMsg = null;
			println("waitForMessage encountered SocketException... shutting down!");
		} catch (Exception e) { //IOException or ClassNotFoundException
			receivedMsg = null;
			println("waitForMessage - " + e.getMessage());
		}

		//System.out.println("Received " + receivedMsg);
		return receivedMsg;
	}

	protected boolean isValidMessage(Message msg) {
		return this.isValidMessage(msg, false);
	}
	
	protected boolean isValidMessage(Message msg, boolean checkContents) {
		if (msg == null) {
			println("Message to be validated was null!");
			return false;
		}
		else if (msg.type.isError) {
			println("Message to be validated is of type " + msg.type);
			return false;
		}
		else if (checkContents && msg.contents == null) {
			println("Message to be validated expected contents, found null!");
			return false;
		}
		return true;
		
	}
	
	protected boolean isOutStreamValid() {
		return outStream != null;
	}
	
	protected boolean isInStreamValid() {
		return inStream != null;
	}
	
	private void println(String s) {
		if (doLog) {
			System.out.println(this.getClass().getSimpleName() + ": " + s);
		}
	}
}
