/*
 * Name: 		NetworkRelay.java
 * Created by:	Max Goad
 * Description:	Class used to connect, send and receive messages from clients via an already established socket.
 */

package com.maxgoad.zombiedice.server.relay;

import java.net.*;

import com.maxgoad.zombiedice.server.message.Message;
import com.maxgoad.zombiedice.server.message.Messenger;

public class Relay extends Messenger implements Runnable {

	private RelayMediator mediator = null;
	private Socket client = null;
	private Thread thread = null;

	private int UID;
	private String clientName;
	
	//////////////
	//Constructors
	//////////////
	public Relay(RelayMediator mediator, Socket client, int UID) {
		this.mediator = mediator;
		this.client = client;
		this.UID = UID;
		this.clientName = null; //This will get set during getClientInfo();
		
		this.thread = new Thread(this);
	}
	
	//////////
	//Main fns
	//////////
	public void start() {
		this.thread.start();
	}
	
	public void run() {
		if (!super.initializeStreams(client)) {
			mediator.reportRelayFailure(this.UID, "super.initializeStreams() failed!");
			return;
		}
		
		if (!establishMessageConnection()) {
			mediator.reportRelayFailure(this.UID, "establishMessageConnection() failed!");
			return;
		}
	}
	
	private boolean establishMessageConnection() {
		
		boolean inputAccepted = false;

		Message request = new Message(Message.Type.CLIENT_REGISTRATION_REQUEST);
		Message response = null;
		
		while (!inputAccepted) {
			//Send request for client info
			if (!sendMessage(request)) {
				return false;
			}

			//Wait for response back from client
			response = waitForMessage(Message.Type.CLIENT_REGISTRATION_RESPONSE);
			
			//If valid, but contents are null, retry
			if (response != null && response.contents == null) {
				request = new Message(Message.Type.ERROR_RETRY_INPUT);
			}
			//Check for generic message validity error
			else if (!isValidMessage(response)) {
				return false;
			}
			//Otherwise, we're good to go
			else {
				inputAccepted = true;
			}
		}
		
		Message confirmation = new Message(Message.Type.CLIENT_REGISTRATION_CONFIRMATION, String.valueOf(this.UID));
		if (!sendMessage(confirmation)) {
			return false;
		}
		
		this.clientName = response.contents;
		this.mediator.addRelay(this);		
		
		return true;
	}
	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	public void setUID(int UID) {
		this.UID = UID;
	}
	
	public int getUID() {
		return this.UID;
	}
	
	public String getClientName() {
		return this.clientName;
	}
}
