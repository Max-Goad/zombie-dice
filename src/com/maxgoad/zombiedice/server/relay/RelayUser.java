package com.maxgoad.zombiedice.server.relay;

public interface RelayUser {
	public void reportRelayFailure(int UID);
}
