package com.maxgoad.zombiedice.server.relay;

import java.net.Socket;
import java.util.*;
import java.util.concurrent.Executors;

import com.maxgoad.zombiedice.server.message.Message;
import com.maxgoad.zombiedice.utils.RandomUtils;
import com.maxgoad.zombiedice.utils.UIDGenerator;

public class RelayMediator {
	
	private RelayUser parent;
	
	private Map<Integer, Relay> relayMap = null;
	
	private Message storedResponse = null;
	private boolean storedResponseReady = false;
	
	private boolean doLog = true;
	
	//////////////
	//Constructors
	//////////////
	public RelayMediator(RelayUser parent) {
		relayMap = new HashMap<>();
		this.parent = parent;
	}
	
	///////////////////
	//Main Mediator Fns
	///////////////////
	public synchronized void sendMessage(int UID, Message msg) {
		Relay relay = getRelay(UID);
		if (relay == null) {
			return;
		}
		
		if (!relay.sendMessage(msg)) {
			Executors.newSingleThreadExecutor().execute(new Runnable() {
				@Override
				public void run() {
					reportRelayFailure(UID, "sendMessage failed!");
				}
			});
		}
		
	}
	
	public synchronized void sendPing(int UID) {
		Relay relay = getRelay(UID);
		if (relay == null) {
			return;
		}
		
		if (!relay.sendPing()) {
			Executors.newSingleThreadExecutor().execute(new Runnable() {
				@Override
				public void run() {
					reportRelayFailure(UID, "sendPing failed!");
				}
			});
		}
	}
	
	public synchronized void broadcastMessage(Message msg) {
		for(int UID : getRelayUIDs()) {
			sendMessage(UID, msg);
		}
	}
	
	public synchronized void broadcastPing() {
		for (int UID : getRelayUIDs()) {
			sendPing(UID);
		}
	}
	
	public Message getResponseFrom(int UID, Message.Type expectedType) {
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(expectedType);
		return this.getResponseFrom(UID, expectedTypes);
	}
	
	public Message getResponseFrom(int UID, Set<Message.Type> expectedTypes) {
		storedResponse = null;
		storedResponseReady = false;
		
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				Relay relay = getRelay(UID);
				if (relay != null) {
					storedResponse = relay.waitForMessage(expectedTypes);
				}
				else {
					reportRelayFailure(UID, "Unable to wait for message - Relay has disconnected!");
				}

				storedResponseReady = true;
			}
		});
		
		//While waiting for the message to come back, ping all clients to ensure they're still connected.
		int counter = 0;
		while (!storedResponseReady) {
			
			RandomUtils.wait(50);

			counter = ((counter + 1) % 20);
			if (counter == 0 ) {
				broadcastPing();
			}
		}
		
		if (storedResponse == null) {
			Executors.newSingleThreadExecutor().execute(new Runnable() {
				@Override
				public void run() {
					reportRelayFailure(UID, "waitForMessage failed!");
				}
			});
		}
		
		return storedResponse;
	}
	
	///////////
	//Relay Fns
	///////////
	public synchronized int createNewRelay(Socket clientSocket) {
		int newUID = UIDGenerator.generate();
		//logger.info("New ID generated! " + newUID);
		Relay newRelay = new Relay(this, clientSocket, newUID);
		newRelay.start();
		return newUID;
	}
	
	public synchronized void addRelay(Relay relay) {		
		relayMap.put(relay.getUID(), relay);
		
		if (doLog) {
			println("Successfully added client \"" + relay.getClientName() + "\" relay!");
		}
	}
	
	public synchronized boolean removeRelay(int UID) {
		if (relayMap.containsKey(UID)) {
			relayMap.remove(UID);
			
			if (doLog) {
				println("Successfully removed client relay with UID = " + UID + "!");
			}
			
			return true;
		}
		else {
			//Key wasn't present in map... something went wrong.
			//One potential cause of this is that the relay hasn't been added yet
			//And failed during initialization
			return false;
		}
	}
	
	public synchronized void reportRelayFailure(int UID, String failureMessage) {
		if (getRelayUIDs().contains(UID)) {
			println("Relay with UID " + UID + " failed with message \"" + failureMessage + "\"");
			parent.reportRelayFailure(UID);
			removeRelay(UID);
		}
	}
 	
	///////////////////////////////////////////
	//Getters, Setters, Other state-related fns
	///////////////////////////////////////////
	public synchronized Set<Integer> getRelayUIDs() {
		return relayMap.keySet();
	}

	public synchronized int getNumRelays() {
		return this.relayMap.size();
	}
	
	public synchronized Relay getRelay(int UID) {
		return relayMap.get(UID);
	}
	
	public String getNameFromUID(int UID) {
		Relay matchingRelay = getRelay(UID);
		if (matchingRelay != null) {
			return matchingRelay.getClientName();
		}
		else {
			//Log error?
			return null;
		}
	}
	
	public void setDoLogFlag(boolean doLog) {
		this.doLog = doLog;
	}
	
	private void println(String s) {
		if (doLog) {
			System.out.println(this.getClass().getSimpleName() + ": " + s);
		}
	}
}

