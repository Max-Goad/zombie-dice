package com.maxgoad.zombiedice.server;

public interface ServerInterface {
	
	
	//Push fns (Will not wait for response)
	public void indicateTurn(int uid);
	public void updateAllStates(String state);
	public void updateState(int uid, String state);
	//public void reportErrorToUser(int uid, String errorMessage);
	
	//Pull fns (Blocking; Waits until response received)
	//public String promptUserForString(int uid, String prompt);
	public String getTurnAction(int uid);
	
}
