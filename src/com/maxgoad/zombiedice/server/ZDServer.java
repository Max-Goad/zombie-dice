package com.maxgoad.zombiedice.server;

import java.io.IOException;
import java.net.*;
import java.util.*;

import com.maxgoad.zombiedice.engine.EngineInterface;
import com.maxgoad.zombiedice.engine.ZDEngine;
import com.maxgoad.zombiedice.server.message.Message;
import com.maxgoad.zombiedice.server.relay.*;
import com.maxgoad.zombiedice.utils.RandomUtils;

public class ZDServer implements ServerInterface, RelayUser {

	public static final byte MIN_PLAYERS = 2;
	public static final byte MAX_PLAYERS = 8;
	public static final byte MIN_ROUNDS  = 1;
	public static final byte MAX_ROUNDS  = 10;
	public static final byte MIN_SCORE   = 13;
	public static final byte MAX_SCORE   = Byte.MAX_VALUE;
	public static final byte MIN_DICE	 = 1;
	public static final byte MAX_DICE	 = 13;
	
	public static final int DEFAULT_PORT_NUMBER  = 5050;
	
	public static void main(String[] args) {
		ZDServer server = new ZDServer();
		server.setSettings(ZDServerSettings.SERVER_LOCAL_TEST);
		server.start();
		
		System.exit(0);
	}
	
	
	///
	
	private RelayMediator 	relayMediator;
	private ZDServerSettings  settings 	 = null;
	private ServerSocket	serverSocket = null;
	private EngineInterface engine		 = null;
	
	private boolean isRunning;
	private boolean doLog;
	
	////////////////
	//Constructors//
	////////////////
	public ZDServer() {
		relayMediator = new RelayMediator(this);
		
		isRunning 	= false;
		doLog 		= true;
	}

	////////////
	//Main fns//
	////////////
	public void start() {
		println("Starting up ZombieDiceServer...");
		
		isRunning = true;
		
		while (this.isRunning) {
			//Step 1: Initialize Server (if not already init'd)
			initialize();
			
			//Step 2: Wait for Players to join
			waitForPlayers();
			
			//Step 3: Initialize Engine
			initializeEngine();
			
			//Step 4: Pass Engine Control until it is finished
			playGame();
			
			//Step 5: Prompt players to play again once game is complete
			promptToPlayAgain();
		}
		
		//Step 6: Stop Server		
		println("Shutting down ZombieDiceServer...");
	}

	public void stop() {
		isRunning = false;
		
		try {
			serverSocket.close();
		} catch (Exception e) {}
	}
	
	@Override
	public void reportRelayFailure(int UID) {
		//TODO - Finish this
		
		//Remove player from game
		engine.removePlayer(UID);
		
		//Notify other players of drop		
		Message dropMessage = new Message(Message.Type.GAME_USER_DROP);
		dropMessage.contents = relayMediator.getNameFromUID(UID) + " has dropped from the game!";
		relayMediator.broadcastMessage(dropMessage);
	}
	
	////////////////////
	//Server Logic Fns//
	////////////////////
	private void initialize() {
		Scanner inputScanner = new Scanner(System.in);
		
		while (settings == null) {
			String s1 = "Please enter the number of players you wish to support ("+MIN_PLAYERS+"-"+MAX_PLAYERS+"):";
			byte iPlayers = getInputByte(inputScanner, s1, MIN_PLAYERS, MAX_PLAYERS);
			
			String s2 = "Please enter the number of rounds you wish to have ("+MIN_ROUNDS+"-"+MAX_ROUNDS+"):";
			byte iRounds = getInputByte(inputScanner, s2, MIN_ROUNDS, MAX_ROUNDS);
			
			String s3 = "Please enter the score needed to win each round ("+MIN_SCORE+"-"+MAX_SCORE+"):";
			byte iScore = getInputByte(inputScanner, s3, MIN_SCORE, MAX_SCORE);
			
			settings = new ZDServerSettings(iPlayers, iRounds, iScore);
			
		}
		
		if (serverSocket == null) {
			try {
				serverSocket = new ServerSocket(DEFAULT_PORT_NUMBER);
				serverSocket.setReuseAddress(true);
			} catch (Exception e) {
				println("Error initializing ZombieDice server!" + e.getMessage());
				this.stop();
				return;
			}
		}
	}

	private void waitForPlayers() {
		
		println("Waiting for players to connect...");
		
		while(this.isRunning() && relayMediator.getNumRelays() != settings.getNumPlayers()) {
			
			Socket newPlayer = null;
			
			try {
				newPlayer = serverSocket.accept();
			} catch (SocketException e) {
				this.stop();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				println(e.getMessage());
				continue;
			}
			
			if (newPlayer != null) {				
				println("Client connection request detected! Verifying...");
				int newUID = relayMediator.createNewRelay(newPlayer);
				
				RandomUtils.wait(500);
				
				relayMediator.broadcastPing();
				
				RandomUtils.wait(500);
				
				if (relayMediator.getRelayUIDs().contains(newUID)) {
					println("New player connected: " + relayMediator.getNameFromUID(newUID) + "(uid:"+ newUID +")");
					connectUpdate();
				}
			}
		}
	}
	
	private void initializeEngine() {
		//Create engine if not already created
		if (engine == null) {
			engine = new ZDEngine(this, settings); 
		}

		engine.resetGameState();
		
		//Register players from list in RelayMediator
		Set<Integer> playerUIDs = relayMediator.getRelayUIDs();
		for (int UID : playerUIDs) {
			engine.addPlayer(UID, relayMediator.getNameFromUID(UID));
		}
		
		println("Engine initialized!");
	}
	
	private void playGame() {
		//Tell all players that the game has started!
		relayMediator.broadcastMessage(new Message(Message.Type.GAME_START));
		
		//Pass control to the Engine
		String winningPlayer = engine.playGame();
		
		Message gameEndMsg = new Message (Message.Type.GAME_END, winningPlayer);
		relayMediator.broadcastMessage(gameEndMsg);
	}
	
	private void promptToPlayAgain() {
		
		//Check with players to see if they want to keep playing
		Set<Integer> playersLeaving = new HashSet<>();
		Set<Message.Type> expectedTypes = new HashSet<>();
		expectedTypes.add(Message.Type.CLIENT_CONTINUE_PLAYING);
		expectedTypes.add(Message.Type.CLIENT_FINISHED_PLAYING);
		
		//Prompt user for play again response
		Set<Integer> playerUIDs = relayMediator.getRelayUIDs();
		for (int UID : playerUIDs) {
			Message response = relayMediator.getResponseFrom(UID, expectedTypes);
			if (response == null || response.type != Message.Type.CLIENT_CONTINUE_PLAYING) {
				playersLeaving.add(UID);
			}
			
			//While we're here, remove all players from gameEngine 
			//(we'll add them back at beginning of loop if they want to play again)
			engine.removePlayer(UID);
		}
		
		//Remove all players that do not want to play again from NetworkMediator
		for (int UID : playersLeaving) {
			relayMediator.removeRelay(UID);
		}
		
		//If no players are playing anymore, stop the server!
		if (relayMediator.getNumRelays() == 0) {
			this.stop();
		}
		
		//Send a connect update to all remaining players
		connectUpdate();
	}
	
	///////////////////////
	//ServerInterface Fns//
	///////////////////////
	@Override
	public void indicateTurn(int uid) {
		Message msg = new Message(Message.Type.GAME_TURN_START);
		relayMediator.sendMessage(uid, msg);
	}
	
	@Override
	public void updateAllStates(String state) {
		Message msg = new Message(Message.Type.GAME_UPDATE_STATE, state);
		relayMediator.broadcastMessage(msg);
	}
	
	@Override
	public void updateState(int uid, String state) {
		Message msg = new Message(Message.Type.GAME_UPDATE_STATE, state);
		relayMediator.sendMessage(uid, msg);
	}
	
	@Override
	public String getTurnAction(int uid) {
		Message request = new Message(Message.Type.GAME_TURN_ACTION_REQUEST);
		relayMediator.sendMessage(uid, request);
		
		Message response = relayMediator.getResponseFrom(uid, Message.Type.GAME_TURN_ACTION_RESPONSE);
		if (response != null && response.type == Message.Type.GAME_TURN_ACTION_RESPONSE) {
			return response.contents;
		}
		else {
			return null;
		}
	}
	
	///////////////////////////
	//Getters/Setters/Helpers//
	///////////////////////////
	public boolean isRunning() {
		return this.isRunning;
	}
	
	public void setSettings(ZDServerSettings settings) {
		this.settings = settings;
	}
	
	private byte getInputByte(Scanner scanner, String prompt, byte min, byte max) {
		
		byte result = Byte.MIN_VALUE;
		
		println(prompt);
		
		while (result == Byte.MIN_VALUE) {
			
			try { 
				result = Byte.parseByte(scanner.nextLine()); 
			}
			catch (NumberFormatException e) {
				println("Cannot parse input! Try again.");
			}
		}
		
		return result;
	}
	
	private void println(String s) {
		if (doLog) {
			System.out.println(this.getClass().getSimpleName() + ": " + s);
		}
	}

	private void connectUpdate() {
		Message connectUpdate = new Message(Message.Type.CLIENT_CONNECT_UPDATE);
		connectUpdate.contents = relayMediator.getNumRelays() + "/" + settings.getNumPlayers();
		relayMediator.broadcastMessage(connectUpdate);
	}
}
