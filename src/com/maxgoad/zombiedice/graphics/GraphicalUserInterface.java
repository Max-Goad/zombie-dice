package com.maxgoad.zombiedice.graphics;

import com.maxgoad.zombiedice.client.ZDClientSettings;

public interface GraphicalUserInterface {

	public void startGame(int uid);
	public boolean stopGame(String winner);
	public void fullQuit();
	
	public ZDClientSettings initialize(ZDClientSettings presets);
	public void updateStartStatus(int numConnected, int numTotal);
	
	public void updateState(String state);
	public void indicateTurn();
	public void indicatePlayerDrop(String msg);
	public String getTurnAction();
}
