package com.maxgoad.zombiedice.graphics;

import java.awt.*;

import javax.swing.*;

import com.maxgoad.zombiedice.client.ZDClient;
import com.maxgoad.zombiedice.client.ZDClientSettings;
import com.maxgoad.zombiedice.engine.ZDGameState;
import com.maxgoad.zombiedice.graphics.window.*;
import com.maxgoad.zombiedice.utils.RandomUtils;

public class ZDUserInterface implements GraphicalUserInterface {

	public static void main(String[] args) {
		
		ZDUserInterface blankUI = new ZDUserInterface(null);
		blankUI.overrideCurrentWindow(ZDMainWindow.class);
		blankUI.updateState(ZDGameState.encryptStateImage(ZDGameState.buildMinimalState()));
		
		ZDUserInterface fullUI = new ZDUserInterface(null);
		fullUI.overrideCurrentWindow(ZDMainWindow.class);
		fullUI.updateState(ZDGameState.encryptStateImage(ZDGameState.buildTestState()));

		ZDUserInterface loginUI = new ZDUserInterface(null);
		loginUI.overrideCurrentWindow(ZDLoginWindow.class);
		
		ZDUserInterface waitUI = new ZDUserInterface(null);
		waitUI.overrideCurrentWindow(ZDWaitWindow.class);
		
		ZDUserInterface endUI = new ZDUserInterface(null);
		endUI.overrideCurrentWindow(ZDEndWindow.class);
	}

	public static final int DEFAULT_FRAME_WIDTH  = 960;
	public static final int DEFAULT_FRAME_HEIGHT = 540;
	public static final int ROLL_PRESSED = 0;
	public static final int QUIT_PRESSED = 1;

	private ZDClient parent = null;
	
	private JFrame frame = null;
	private ZDWindow currentWindow 	  = null;
	private ZDLoginWindow loginWindow = null;
	private ZDWaitWindow waitWindow   = null;
	private ZDMainWindow mainWindow   = null;
	private ZDEndWindow endWindow	  = null;
	private ZDClientSettings settings = null;
	
	private int uid = -1;
	private int turnResponse = -1;
	
	private boolean doLog = true;
	
	////////////////
	//Constructors//
	////////////////
	public ZDUserInterface(ZDClient parent) {
		
		this.parent = parent;
		
		this.frame = new JFrame("Zombie Dice");
		this.loginWindow = new ZDLoginWindow(this);
		this.waitWindow  = new ZDWaitWindow(this);
		this.mainWindow  = new ZDMainWindow(this);
		this.endWindow   = new ZDEndWindow(this);
		
		this.currentWindow = loginWindow;
		
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
	}

	////////////
	//Main fns//
	////////////
	@Override
	public void startGame(int uid) {
		this.uid = uid;
		
		changeCurrentWindow(mainWindow);
	}

	@Override
	public boolean stopGame(String winner) {
		
		if (winner == null) {
			JOptionPane.showMessageDialog(frame, "The server has encountered an error. Please reload and try again!", "Server Error", JOptionPane.ERROR_MESSAGE);
			fullQuit();
			return true;
		}
		
		boolean result = true;
		
		JOptionPane.showMessageDialog(frame, "The game has ended! " + winner + " has won!");
		
		endWindow.updateWithWinner(winner);
		
		changeCurrentWindow(endWindow);
		
		while(this.turnResponse == -1) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (turnResponse == 0) {
			result = false;
			turnResponse = -1;
		}
		else {
			fullQuit();
		}
		
		return result;
	}
	
	@Override
	public void fullQuit() {
		parent.fullQuit();
		frame.dispose();
	}
	
	@Override
	public ZDClientSettings initialize(ZDClientSettings presets) {
		
		if (presets != null) {
			loginWindow.updateWithPresets(presets);
		}
		
		changeCurrentWindow(loginWindow);
		
		while(this.settings == null || !this.settings.complete()) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		return this.settings;
	}
	
	@Override
	public void updateStartStatus(int numConnected, int numTotal) {
		
		popCurrentWindow();
		
		waitWindow.updateWindow(numConnected, numTotal);
		
		changeCurrentWindow(waitWindow);
	}

	@Override
	public void updateState(String state) {		
		//Remove window to prepare for changes
		//frame.remove(mainWindow);
		
		if (!verifyCurrentWindow(mainWindow)) {
			return;
		}
		
		//Update window
		ZDGameState currentState = ZDGameState.decryptStateImage(state);
		mainWindow.updatePanels(uid, currentState);
		
		//Custom frame repainting because I suck
		frame.remove(currentWindow);
		
		this.currentWindow = mainWindow;
		
		frame.add(currentWindow);
		frame.revalidate();
		frame.repaint();
	}

	@Override
	public void indicateTurn() {
		JOptionPane.showMessageDialog(frame, "Your turn has begun!");
	}
	
	@Override
	public void indicatePlayerDrop(String msg) {
		JOptionPane.showMessageDialog(frame, msg, "Player Drop Message", JOptionPane.WARNING_MESSAGE);
	}
	
	@Override
	public String getTurnAction() {
		String result = "";
		
		mainWindow.setActive(true);
		
		//While waiting for the turn action, ping server to ensure it's still connected
		int counter = 0;
		while(this.turnResponse != ROLL_PRESSED && this.turnResponse != QUIT_PRESSED) {
			
			RandomUtils.wait(50);

			counter = ((counter + 1) % 20);
			if (counter == 0 && !parent.sendPing()) {
				println("Ping to server during getTurnAction() failed!"); 
				return null;
			}
		}
		mainWindow.setActive(false);
		
		if (this.turnResponse == ROLL_PRESSED) {
			result = "ROLL";
		}
		else if (this.turnResponse == QUIT_PRESSED) {
			result = "QUIT";
		}

		
		this.turnResponse = -1;
		
		return result;
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	public void setTurnResponse(int i) {
		this.turnResponse = i;
	}
	
	public ZDClientSettings getClientSettings() {
		return this.settings;
	}
	
	public void setClientSettings(ZDClientSettings settings) {
		this.settings = settings;
	}
	
	public void overrideCurrentWindow(Class<?> w) {
		switch(w.getSimpleName()) {
			case "ZDMainWindow":
				changeCurrentWindow(mainWindow);
				break;
			case "ZDLoginWindow":
				changeCurrentWindow(loginWindow);
				break;
			case "ZDWaitWindow":
				changeCurrentWindow(waitWindow);
				break;
			case "ZDEndWindow":
				changeCurrentWindow(endWindow);
				break;
			default:
				println("No override for: " + w.getSimpleName());
				break;
		}
	}
	
	private void popCurrentWindow() {
		if (currentWindow != null) {
			frame.remove(currentWindow);
			frame.setVisible(false);
		}
	}
	
	private void pushCurrentWindow() {
		frame.add(currentWindow);
		frame.pack();
		frame.setVisible(true);
	}
	
	private void changeCurrentWindow(ZDWindow w) {
		popCurrentWindow();		
		this.currentWindow = w;
		pushCurrentWindow();
	}
	
	private boolean verifyCurrentWindow(ZDWindow w) {
		return w.equals(currentWindow);
	}
	
	private void println(String s) {
		if (doLog) {
			System.out.println(this.getClass().getSimpleName() + ": " + s);
		}
	}
}
