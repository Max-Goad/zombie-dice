package com.maxgoad.zombiedice.graphics.window;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;

import com.maxgoad.zombiedice.graphics.ZDUserInterface;

public class ZDEndWindow extends ZDWindow {
	
	private static final long serialVersionUID = -8718722625528230317L;
	
	private JPanel mainPanel = null;
	
	public static void main(String[] args) {
		JFrame testFrame = new JFrame();
		testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		testFrame.setLayout(new BorderLayout());
		testFrame.add(new ZDEndWindow(new ZDUserInterface(null)));
		testFrame.pack();
		testFrame.setLocationRelativeTo(null);
		testFrame.setResizable(false);
		testFrame.setVisible(true);
	}

	////////////////
	//Constructors//
	////////////////
	public ZDEndWindow(ZDUserInterface parent) {
		super(parent);
		
		//setLayout(new GridBagLayout());
		
		mainPanel = createMainPanel();
		addPanelsToWindow();
	}

	////////////
	//Main fns//
	////////////
	public void updateWithWinner(String winner) {
		remove(mainPanel);
		
		mainPanel = createMainPanel(winner);
		
		addPanelsToWindow();
	}
	
	private void addPanelsToWindow() {
		
		add(mainPanel);
		
		this.revalidate();
		this.repaint();
		this.setVisible(true);
	}
	
	private void buttonPressed(boolean b) {
		if (b) {
			parent.setTurnResponse(0);
		}
		else {
			parent.setTurnResponse(1);
		}
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////	
 	private JPanel createMainPanel() {
 		return createMainPanel(null);
 	}
 		
	private JPanel createMainPanel(String winner) {
		///Create Panel Base
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(30,30,30,30));
		panel.setBackground(BACKGROUND_COLOUR);
		panel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH/3, ZDUserInterface.DEFAULT_FRAME_HEIGHT/2));
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10,0,10,0), 0, 0);
		
		///Create Blank Components
		JLabel nameLabel = null;
		
		JButton yesButton = null,
				noButton  = null;
		
		///Populate Components
		
		//Labels
		nameLabel = new JLabel(winner + " was the winner! Do you want to play again?");
		nameLabel.setHorizontalAlignment(JLabel.CENTER);
		
		//Buttons
		yesButton = new JButton("Yes");
		noButton  = new JButton("No");

		yesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonPressed(true);
			}
		});
		noButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonPressed(false);
			}
		});
		
		Font buttonFont = new Font("Arial", Font.BOLD, 16);
		yesButton.setFont(buttonFont);
		noButton.setFont(buttonFont);
		
		///Add Components to Panel
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 2;
		panel.add(nameLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.weighty = 1;
		panel.add(yesButton, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 1;
		panel.add(noButton, gbc);
		
		return panel;
	}
}
