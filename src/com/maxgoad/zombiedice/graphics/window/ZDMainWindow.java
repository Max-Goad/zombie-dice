package com.maxgoad.zombiedice.graphics.window;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;

import com.maxgoad.zombiedice.engine.*;
import com.maxgoad.zombiedice.engine.ZDice.*;
import com.maxgoad.zombiedice.graphics.ZDUserInterface;

public class ZDMainWindow extends ZDWindow {

	private static final long serialVersionUID = 456709532016024195L;
	
	private JPanel leftPanel = null;
	private JPanel rightPanel = null;
	
	private boolean isActive = false;

	////////////////
	//Constructors//
	////////////////
	public ZDMainWindow(ZDUserInterface parent) {
		super(parent);
		
		setLayout(new GridBagLayout());
		
		//Create Left Side
		leftPanel = createLeftPanel();
		
		//Create Right Side
		rightPanel = createRightPanel();
		
		//Arrange and add panels
		addPanelsToWindow();
	}

	////////////
	//Main fns//
	////////////
 	public void updatePanels(int yourUID, ZDGameState gameState) {

		updateLeftPanel(yourUID, gameState);
		
		updateRightPanel(yourUID, gameState.getCurrentPlayerUID(), gameState.getPlayers());
		
		addPanelsToWindow();
		
	}
	
	private void updateLeftPanel(int yourUID, ZDGameState gameState) {
		leftPanel.removeAll();
		
		leftPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0, 0);
		
		int diceBuffer = DICE_SMALL_SIZE/10;
		LineBorder lineBorder = new LineBorder(DEFAULT_LEFT_COLOUR.darker(), diceBuffer);
		
		//Score Panel
		JPanel scorePanel = new JPanel();
		scorePanel.setLayout(new GridLayout(5, 3, diceBuffer, diceBuffer));
		scorePanel.setOpaque(false);
		scorePanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Score"));
		scorePanel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*2/9, ZDUserInterface.DEFAULT_FRAME_HEIGHT*5/7));
		for (int i = 0; i < SCORE_MAX_DICE; i++) {
			ImageIcon img = null;
			if (i < gameState.getScoreDice().size()) {
				ZDice d = gameState.getScoreDice().get(i);
				img = loadDiceImage(d.getColour(), d.getCurrentFace(), DICE_SMALL_SIZE);
			}
			else {
				img = loadEmptyImage(DICE_SMALL_SIZE);
			}

			scorePanel.add(new JLabel(img));
			
		}

		gbc.gridx 		= 0;
		gbc.gridy 		= 0;
		gbc.gridwidth 	= 1;
		gbc.gridheight 	= 5;
		gbc.weightx = 1;
		leftPanel.add(scorePanel, gbc);
		
		//Two JButtons
		Font buttonFont = new Font("Arial", Font.BOLD, 24);
		
		JButton rollButton = new JButton("Roll 3 Dice");
		rollButton.setFont(buttonFont);
		rollButton.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*2/9, ZDUserInterface.DEFAULT_FRAME_HEIGHT*1/7));
		rollButton.setEnabled(yourUID == gameState.getCurrentPlayerUID());
		rollButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rollButtonPressed();
			}
		});
		gbc.gridy = 5;
		gbc.gridheight = 1;
		leftPanel.add(rollButton, gbc);
		
		JButton quitButton = new JButton("Quit");
		quitButton.setFont(buttonFont);
		quitButton.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*2/9, ZDUserInterface.DEFAULT_FRAME_HEIGHT*1/7));
		quitButton.setEnabled(yourUID == gameState.getCurrentPlayerUID());
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitButtonPressed();
			}
		});
		gbc.gridy = 6;
		leftPanel.add(quitButton, gbc);
		
		//Info Panel
		JPanel infoPanel = new JPanel();
		infoPanel.setOpaque(false);
		infoPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Info"));
		infoPanel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*2/9, ZDUserInterface.DEFAULT_FRAME_HEIGHT*1/7));

		String textfield = "Zombie Dice!";
		ZDPlayer p = gameState.getCurrentPlayer();
		if (p != null) {
			textfield = gameState.getCurrentPlayer().getName() + " - " + gameState.getScoreToWin();
		}
		JLabel text = new JLabel(textfield);
		text.setHorizontalAlignment(SwingConstants.CENTER);
		text.setVerticalAlignment(SwingConstants.CENTER);
		text.setFont(buttonFont);
		infoPanel.add(text);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth  = 1;
		gbc.gridheight = 1;
		gbc.weighty = 1/7;
		leftPanel.add(infoPanel, gbc);
		
		//Death Panel
		JPanel deathPanel = new JPanel();
		deathPanel.setLayout(new GridLayout(1, 3, diceBuffer, diceBuffer));
		deathPanel.setOpaque(false);
		deathPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Blast Count"));
		deathPanel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*2/9, ZDUserInterface.DEFAULT_FRAME_HEIGHT*1/7));
		for (int i = 0; i < DEATH_MAX_DICE; i++) {
			ImageIcon img = null;
			if (i < gameState.getDeathDice().size()) {
				ZDice d = gameState.getDeathDice().get(i);
				img = loadDiceImage(d.getColour(), d.getCurrentFace(), DICE_SMALL_SIZE);
			}
			else {
				img = loadEmptyImage(DICE_SMALL_SIZE);
			}
			
			deathPanel.add(new JLabel(img));
		}
		
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weighty = 1/7;
		leftPanel.add(deathPanel, gbc);
		
		//Game Field Panel
		JPanel fieldPanel = new JPanel();
		fieldPanel.setLayout(new GridLayout(1, 3, diceBuffer, diceBuffer));
		fieldPanel.setOpaque(false);
		fieldPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Current Roll"));
		fieldPanel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*4/9, ZDUserInterface.DEFAULT_FRAME_HEIGHT*6/7));
		for (ZDice d : gameState.getFieldDice()) {
			ImageIcon img = loadDiceImage(d.getColour(), d.getCurrentFace(), DICE_LARGE_SIZE);
			JLabel imgLabel = new JLabel(img);
			fieldPanel.add(imgLabel);
		}
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		gbc.gridheight = 6;
		gbc.weightx = 2;
		gbc.weighty = 6/7;
		leftPanel.add(fieldPanel, gbc);
		
		
		leftPanel.revalidate();
		leftPanel.repaint();
	}
	
	private void updateRightPanel(int yourUID, int currentUID, ArrayList<ZDPlayer> players) {
		rightPanel.removeAll();
		
		rightPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0);
		
		Font header = new Font("Arial", Font.BOLD, 18);
		
		//Name Header
		JLabel nameHeader  = new JLabel("Player Name");
		nameHeader.setHorizontalAlignment(SwingConstants.CENTER);
		nameHeader.setFont(header);
		gbc.gridx = 0;
		rightPanel.add(nameHeader, gbc);
		
		//Wins Header
		JLabel winsHeader  = new JLabel("Wins");
		winsHeader.setHorizontalAlignment(SwingConstants.CENTER);
		winsHeader.setFont(header);
		gbc.gridx = 1;
		rightPanel.add(winsHeader, gbc);
		
		//Score Header
		JLabel scoreHeader = new JLabel("Score");
		scoreHeader.setHorizontalAlignment(SwingConstants.CENTER);
		scoreHeader.setFont(header);
		gbc.gridx = 2;
		rightPanel.add(scoreHeader, gbc);
		
		//Add Player Info Labels
		for (int i = 0; i < players.size(); i++) {
			ZDPlayer p = players.get(i);
			
			//Font Styling
			int fontStyle = 0;
			if (p.getUid() == yourUID) {
				fontStyle += Font.ITALIC;
			}
			if (p.getUid() == currentUID) {
				fontStyle += Font.BOLD;
			}
			if (fontStyle < 1) {
				fontStyle = Font.PLAIN;
			}
			
			Font f = new Font("Arial", fontStyle, 14);
			//End Font
			
			gbc.gridy = i+1;
			
			//Player Name
			JLabel name  = new JLabel(p.getName());
			name.setHorizontalAlignment(SwingConstants.CENTER);
			name.setFont(f);
			gbc.gridx = 0;
			rightPanel.add(name, gbc);
			
			//Player Wins
			JLabel wins  = new JLabel(Byte.toString(p.getWins()));
			wins.setHorizontalAlignment(SwingConstants.CENTER);
			wins.setFont(f);
			gbc.gridx = 1;
			rightPanel.add(wins, gbc);
			
			//Player Score
			JLabel score = new JLabel(Byte.toString(p.getScore()));
			score.setHorizontalAlignment(SwingConstants.CENTER);
			score.setFont(f);
			gbc.gridx = 2;
			rightPanel.add(score, gbc);
		}
		
		//Buffer
		JLabel empty = new JLabel("");
		gbc.gridx = 0;
		gbc.gridy = players.size()+1;
		gbc.gridwidth = 3;
		gbc.weighty = 10;
		rightPanel.add(empty, gbc);
		
		//Info Labels
		JLabel info1 = new JLabel("A bolded row indicates the player whose turn it is.");
		info1.setFont(new Font("Arial", Font.BOLD, 9));
		info1.setHorizontalAlignment(SwingConstants.CENTER);
		info1.setVerticalAlignment(SwingConstants.BOTTOM);
		gbc.gridy++;
		gbc.weighty = 0.2;
		rightPanel.add(info1, gbc);
		
		JLabel info2 = new JLabel("An italic row indicates your player information.");
		info2.setFont(new Font("Arial", Font.ITALIC, 9));
		info2.setHorizontalAlignment(SwingConstants.CENTER);
		info2.setVerticalAlignment(SwingConstants.BOTTOM);
		gbc.gridy++;
		rightPanel.add(info2, gbc);
		 
		
		rightPanel.revalidate();
		rightPanel.repaint();
	}
	
	private void addPanelsToWindow() {
		
		remove(leftPanel);
		remove(rightPanel);
		
		GridBagConstraints leftConstraints = new GridBagConstraints();
		leftConstraints.gridx = 0;
		leftConstraints.gridy = 0;
		leftConstraints.weightx = 2;
		leftConstraints.weighty = 1;
		leftConstraints.fill = GridBagConstraints.BOTH;
		
		GridBagConstraints rightConstraints = new GridBagConstraints();
		rightConstraints.gridx = 1;
		rightConstraints.gridy = 0;
		rightConstraints.weightx = 1;
		rightConstraints.weighty = 1;
		rightConstraints.fill = GridBagConstraints.BOTH;
		
		add(leftPanel, leftConstraints);
		add(rightPanel, rightConstraints);

		this.revalidate();
		this.repaint();
	}
	
	private ImageIcon loadDiceImage(ZDColour c, ZDFace f, int size) {
		ImageIcon result = null;
		String fileName = "/"+c+"_"+f+".png";
		
		try {
			BufferedImage originalImage = ImageIO.read(getClass().getResource(fileName));
			Image scaledImage = originalImage.getScaledInstance(size, size, Image.SCALE_SMOOTH);
			result = new ImageIcon(scaledImage);
		} catch (IOException e) {}
		
		return result;
	}
	
	private ImageIcon loadEmptyImage(int size) {
		BufferedImage createdImage = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		return new ImageIcon(createdImage);
	}
	
	private void rollButtonPressed() {
		System.out.println("Roll Button Pressed!");
		if (isActive) {
			System.out.println("Passing Roll Message!");
			parent.setTurnResponse(ZDUserInterface.ROLL_PRESSED);
		}
	}
	
	private void quitButtonPressed() {
		System.out.println("Quit Button Pressed!");
		if (isActive) {
			System.out.println("Passing Quit Message!");
			parent.setTurnResponse(ZDUserInterface.QUIT_PRESSED);
		}
	}
	///////////////////
	//Getters/Setters//
	///////////////////
	public void setActive(boolean active) {
		this.isActive = active;
	}
	
	private JPanel createLeftPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10,10,10,10));
		panel.setBackground(DEFAULT_LEFT_COLOUR);
		panel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH*2/3, ZDUserInterface.DEFAULT_FRAME_HEIGHT));
		
		return panel;
	}
	
	private JPanel createRightPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10,10,10,10));
		panel.setBackground(DEFAULT_RIGHT_COLOUR);
		panel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH/3, ZDUserInterface.DEFAULT_FRAME_HEIGHT));
		
		return panel;
	}
}
