package com.maxgoad.zombiedice.graphics.window;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.MaskFormatter;

import com.maxgoad.zombiedice.client.ZDClientSettings;
import com.maxgoad.zombiedice.graphics.ZDUserInterface;

public class ZDLoginWindow extends ZDWindow {

	private static final long serialVersionUID = 7005853143247751243L;
	
	private JPanel mainPanel = null;
	
	private JTextField			nameField = null;
	private JFormattedTextField	ipField   = null;
	private JTextField			portField = null;
	
	public static void main(String[] args) {
		JFrame testFrame = new JFrame();
		testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		testFrame.setLayout(new BorderLayout());
		testFrame.add(new ZDLoginWindow(new ZDUserInterface(null)));
		testFrame.pack();
		testFrame.setLocationRelativeTo(null);
		testFrame.setResizable(false);
		testFrame.setVisible(true);
	}

	////////////////
	//Constructors//
	////////////////
	public ZDLoginWindow(ZDUserInterface parent) {
		super(parent);
		
		//setLayout(new GridBagLayout());
		
		mainPanel = createMainPanel();
		addPanelsToWindow();
	}

	////////////
	//Main fns//
	////////////
	public void updateWithPresets(ZDClientSettings presets) {
		remove(mainPanel);
		
		mainPanel = createMainPanel(presets);
		
		addPanelsToWindow();
	}
	
	private void addPanelsToWindow() {
		
		add(mainPanel);
		
		this.revalidate();
		this.repaint();
		this.setVisible(true);
	}
	
	private void connectButtonPressed(String name, String ip, String port) {
		if (name.length() < 1) {
			createErrorDialog("You must enter in a name!");
			return;
		}
		
		if (name.length() > MAX_NAME_LENGTH) {
			createErrorDialog("Your name is too long! (" + MAX_NAME_LENGTH + " character maximum)");
			return;
		}
		
		if (port.length() < 1) {
			createErrorDialog("You must enter in a valid port number!");
			return;
		}
		
		if (port.length() > MAX_PORT_LENGTH) {
			createErrorDialog("Your port number is too long! (" + MAX_PORT_LENGTH + " number maximum)");
			return;
		}
		
		int portNum = -1;
		try {
			portNum = Integer.parseInt(port);
		} catch (NumberFormatException e) {
			createErrorDialog("You've entered an invalid port number!");
			return;
		}
		
		parent.setClientSettings(new ZDClientSettings(name, ip, portNum));
		
	}
	
	private void quitButtonPressed() {
		parent.fullQuit();
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////	
 	private JPanel createMainPanel() {
 		return createMainPanel(null);
 	}
 		
	private JPanel createMainPanel(ZDClientSettings presets) {
		///Create Panel Base
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(30,30,30,30));
		panel.setBackground(BACKGROUND_COLOUR);
		panel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH/3, ZDUserInterface.DEFAULT_FRAME_HEIGHT/2));
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10,0,10,0), 0, 0);
		
		///Create Blank Components
		JLabel nameLabel = null,
			   ipLabel   = null,
			   portLabel = null;
		
		MaskFormatter  ipFormat   = null;

		nameField = null;
		ipField   = null;
		portField = null;
		
		JButton connectButton = null,
				quitButton	  = null;
		
		///Populate Components
		
		//Labels
		nameLabel = new JLabel("Name: ");
		ipLabel   = new JLabel("Server IP: ");
		portLabel = new JLabel("Port Number: ");
		
		nameLabel.setHorizontalAlignment(JLabel.LEFT);
		ipLabel.setHorizontalAlignment(JLabel.LEFT);
		portLabel.setHorizontalAlignment(JLabel.LEFT);
		
		//TF Formats
		try {
			ipFormat   = new MaskFormatter("###.###.###.###");
			ipFormat.setAllowsInvalid(false);
			ipFormat.setPlaceholderCharacter('0');
		} catch (ParseException e) {
			// TODO Figure out acceptable
			e.printStackTrace();
		}
		
		//TextFields
		nameField = new JTextField();
		ipField   = new JFormattedTextField(ipFormat);
		portField = new JTextField();
		
		if (presets != null) {
			nameField.setText(presets.getPlayerName());
			ipField.setValue(presets.getServerIP());
			portField.setText("" + presets.getServerPort());
		}
		
		//Buttons
		connectButton = new JButton("Connect");
		quitButton    = new JButton("Quit");

		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connectButtonPressed(nameField.getText(), ipField.getText(), portField.getText());
			}
		});
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitButtonPressed();
			}
		});
		
		Font buttonFont = new Font("Arial", Font.BOLD, 16);
		connectButton.setFont(buttonFont);
		quitButton.setFont(buttonFont);
		
		///Add Components to Panel
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		panel.add(nameLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.weighty = 1;
		panel.add(ipLabel, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.weighty = 1;
		panel.add(portLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.weightx = 1;
		gbc.weighty = 1;
		panel.add(nameField, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		panel.add(ipField, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		panel.add(portField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		panel.add(connectButton, gbc);

		gbc.gridx = 2;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		panel.add(quitButton, gbc);
		
		return panel;
	}
}
