package com.maxgoad.zombiedice.graphics.window;

import java.awt.Color;

import javax.swing.*;

import com.maxgoad.zombiedice.graphics.ZDUserInterface;

public class ZDWindow extends JPanel {

	private static final long serialVersionUID = -1622496395557983141L;

	protected static final Color BACKGROUND_COLOUR  = new Color(250, 200, 200);
	protected static final int MAX_NAME_LENGTH = 16;
	protected static final int MAX_PORT_LENGTH = 6;
	
	protected static final int DICE_SMALL_SIZE = 50;
	protected static final int DICE_LARGE_SIZE = 100;
	protected static final int SCORE_MAX_DICE  = 15;
	protected static final int DEATH_MAX_DICE  = 3;
	
	protected static final Color DEFAULT_LEFT_COLOUR  = new Color(250, 200, 200);
	protected static final Color DEFAULT_RIGHT_COLOUR = new Color(200, 250, 200);
	
	protected ZDUserInterface parent = null;
	
	////////////////
	//Constructors//
	////////////////
	public ZDWindow(ZDUserInterface parent) {
		this.parent = parent;
	}

	////////////
	//Main fns//
	////////////
	protected void createErrorDialog(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
	}

	///////////////////
	//Getters/Setters//
	///////////////////

}
