package com.maxgoad.zombiedice.graphics.window;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;

import com.maxgoad.zombiedice.graphics.ZDUserInterface;

public class ZDWaitWindow extends ZDWindow {
	
	private static final long serialVersionUID = 7326049293249272967L;
	
	private JPanel mainPanel = null;
	
	public static void main(String[] args) {
		JFrame testFrame = new JFrame();
		testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		testFrame.setLayout(new BorderLayout());
		testFrame.add(new ZDWaitWindow(new ZDUserInterface(null)));
		testFrame.pack();
		testFrame.setLocationRelativeTo(null);
		testFrame.setResizable(false);
		testFrame.setVisible(true);
	}

	////////////////
	//Constructors//
	////////////////
	public ZDWaitWindow(ZDUserInterface parent) {
		super(parent);
		
		//setLayout(new GridBagLayout());
		
		updateWindow(0,0);
	}

	////////////
	//Main fns//
	////////////
	public void updateWindow(int playersConnected, int playersTotal) {

		if (mainPanel != null) {
			remove(mainPanel);
		}
		
		mainPanel = createMainPanel(playersConnected, playersTotal);
		
		add(mainPanel);
		
		this.revalidate();
		this.repaint();
	}
	
	private void quitButtonPressed() {
		parent.fullQuit();
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////	
 	private JPanel createMainPanel(int playersConnected, int playersTotal) {
		///Create Panel Base
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(30,30,30,30));
		panel.setBackground(BACKGROUND_COLOUR);
		panel.setPreferredSize(new Dimension(ZDUserInterface.DEFAULT_FRAME_WIDTH/3, ZDUserInterface.DEFAULT_FRAME_HEIGHT/2));
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10,0,10,0), 0, 0);
		
		///Create Blank Components
		JLabel infoLabel1 = null,
			   infoLabel2 = null,
			   infoLabel3 = null;
		
		JButton quitButton = null;
		
		///Populate Components
		
		//Labels
		infoLabel1 = new JLabel("You are now connected to the server!");
		infoLabel2 = new JLabel("(" + playersConnected + "/" + playersTotal + ") players currently connected.");
		infoLabel3 = new JLabel("Please wait for the remaining players to connect!");
		
		infoLabel1.setHorizontalAlignment(JLabel.CENTER);
		infoLabel2.setHorizontalAlignment(JLabel.CENTER);
		infoLabel3.setHorizontalAlignment(JLabel.CENTER);
		
		//Buttons
		quitButton    = new JButton("Quit");

		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitButtonPressed();
			}
		});
		
		Font buttonFont = new Font("Arial", Font.BOLD, 16);
		quitButton.setFont(buttonFont);
		
		///Add Components to Panel
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		panel.add(infoLabel1, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.weighty = 1;
		panel.add(infoLabel2, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.weighty = 1;
		panel.add(infoLabel3, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		panel.add(quitButton, gbc);
		
		return panel;
	}
}
